﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Console.Extension;
using Console.ProcessExcel.Data;
using Console.ProcessExcel.Services;
using LinqToExcel;

namespace Console.ProcessExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                @"Files/Event list Vic updates.xlsx");
            var events = ParseEvents(path);
            events.ForEach(@event =>
            {
                System.Console.WriteLine($"Event ID: {@event.GameID}, {@event.EventDescription}");
            });
            System.Console.ReadLine();
            //UpdateEventDescription(@events).GetAwaiter().GetResult();
            //System.Console.WriteLine("Finish");
            //System.Console.ReadLine();
            //var filename = "GameList-22112018.xlsx";
            //var from = DateTime.Today.StartOfWeek(DayOfWeek.Monday);
            //var to = DateTime.Today.EndOfWeek(DayOfWeek.Sunday);
            //WriteGameListExcel(filename, from, to);
        }

        static List<Event> ParseEvents(string filename)
        {
            var excel = new ExcelQueryFactory(filename);
            var events = excel.Worksheet<Event>().ToList();
            return events;
        }

        static async Task UpdateEventDescription(List<Event> events)
        {
            var ids = events.Select(p => p.GameID).ToList();
            using (var context = new FHGAdminEntities())
            {
                var entities = await context.evt_Game.Where(p => ids.Any(i => i == p.GameID)).Select(p => new
                {
                    GameId = p.GameID,
                    Event = p.evt_Event
                }).ToListAsync();
                if (entities.Any())
                {
                    System.Console.WriteLine($"Found {entities.Count} events");
                    foreach (var entity in entities)
                    {
                        //get updated event info from excel file
                        var excelEvt = events.FirstOrDefault(p => p.GameID == entity.GameId);
                        if (excelEvt == null) continue;

                        if (entity.Event.EventDate.HasValue)
                        {
                            System.Console.WriteLine($"Looking for all current season events for Venue: {entity.Event.OrgEntityID}, Event Date: {entity.Event.EventDate.Value.ToShortDateString()}, Start Time: {entity.Event.StartTime}");
                            //get current event day of week
                            //using this info to update all the other events for the current season
                            var dayofWeek = (int)entity.Event.EventDate.Value.DayOfWeek + 1;
                            var sameEvents = await context.evt_Event.Where(p =>
                                                                           //same venue
                                                                           p.OrgEntityID == entity.Event.OrgEntityID &&
                                                                           //same season
                                                                           p.SeasonID == entity.Event.SeasonID &&
                                                                           //not archieved
                                                                           p.Archived == false &&
                                                                           //same day of week (i.e. Monday)
                                                                           SqlFunctions.DatePart("dw", p.EventDate) == dayofWeek &&
                                                                           //same start time
                                                                           p.StartTime.Equals(excelEvt.FormattedStartTime)).ToListAsync();
                            System.Console.WriteLine($"Found events {sameEvents.Count}");
                            foreach (var @event in sameEvents)
                            {
                                @event.EventDescription = excelEvt.EventDescription;
                                @event.ModificationDate = DateTime.Now;
                                //michael ratnik
                                @event.ModifiedByID = 260;
                            }
                            System.Console.WriteLine("==========================================================");
                        }
                    }

                    await context.SaveChangesAsync();
                }
            }
        }

        static void WriteGameListExcel(string filename, DateTime from, DateTime to)
        {
            var repo = new EventRepository();
            var excelSvc = new ExcelService(filename);
            var events = repo.GetEventListDateRange(from, to);
            excelSvc.WriteExcel(events);
        }
    }
}
