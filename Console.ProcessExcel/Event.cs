﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console.ProcessExcel
{
    public class Event
    {
        public string VenueName { get; set; }
        public DateTime? EventDate { get; set; }
        public string StartTime { get; set; }
        public int GameID { get; set; }
        public string EventDescription { get; set; }
        public decimal? BuyinFee { get; set; }
        public decimal? AdminFee { get; set; }
        public decimal? ProFee { get; set; }
        public string DivisionName { get; set; }

        public string GameTypeName { get; set; }
        public string State { get; set; }
        public string RegionName { get; set; }
        public string FormattedStartTime => StartTime;
    }
}
