﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Console.ProcessExcel.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class FHGAdminEntities : DbContext
    {
        public FHGAdminEntities()
            : base("name=FHGAdminEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<evt_Event> evt_Event { get; set; }
        public virtual DbSet<evt_Game> evt_Game { get; set; }
        public virtual DbSet<evt_Season> evt_Season { get; set; }
        public virtual DbSet<evt_Discipline> evt_Discipline { get; set; }
        public virtual DbSet<view_evt_EventInfo> view_evt_EventInfo { get; set; }
    }
}
