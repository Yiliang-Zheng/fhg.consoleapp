//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Console.ProcessExcel.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class evt_Game
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public evt_Game()
        {
            this.view_evt_EventInfo = new HashSet<view_evt_EventInfo>();
        }
    
        public int GameID { get; set; }
        public System.Guid GameGUID { get; set; }
        public string GameName { get; set; }
        public Nullable<int> DisciplineID { get; set; }
        public Nullable<int> TournamentTypeID { get; set; }
        public Nullable<int> ResultOptionID { get; set; }
        public Nullable<int> GameTypeID { get; set; }
        public Nullable<int> ScoreTypeID { get; set; }
        public Nullable<int> GameStatusID { get; set; }
        public Nullable<int> DivisionID { get; set; }
        public Nullable<int> EventID { get; set; }
        public Nullable<decimal> GameMultiplier { get; set; }
        public string GameDescription { get; set; }
        public Nullable<decimal> PlayerAdminFee { get; set; }
        public Nullable<decimal> PlayerBuyInFee { get; set; }
        public Nullable<decimal> PlayerEntryFee { get; set; }
        public Nullable<decimal> PlayerProFee { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<bool> PointsFlow { get; set; }
        public Nullable<bool> GenerateInvoice { get; set; }
        public Nullable<int> GameScoreboardID { get; set; }
        public string StartTime { get; set; }
        public string RegistrationTime { get; set; }
        public Nullable<decimal> RegistrationStartHoursBefore { get; set; }
        public Nullable<int> PassportCount { get; set; }
        public Nullable<int> ITTables { get; set; }
        public Nullable<bool> Archived { get; set; }
        public System.DateTime CreationDate { get; set; }
        public int CreatedByID { get; set; }
        public Nullable<System.DateTime> ModificationDate { get; set; }
        public Nullable<int> ModifiedByID { get; set; }
        public string TournamentID { get; set; }
        public System.Guid UpdateVersion { get; set; }
        public Nullable<bool> LimitedRegistration { get; set; }
        public Nullable<bool> ReservePlayer { get; set; }
    
        public virtual evt_Event evt_Event { get; set; }
        public virtual evt_Discipline evt_Discipline { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<view_evt_EventInfo> view_evt_EventInfo { get; set; }
    }
}
