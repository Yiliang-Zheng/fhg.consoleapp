﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console.ProcessExcel.Data
{
    public class EventRepository
    {
        public List<Event> GetEventListDateRange(DateTime from, DateTime to)
        {
            using (var context = new FHGAdminEntities())
            {
                var result = context.view_evt_EventInfo.Where(p => p.EventDate >= from && p.EventDate <= to).Select(p =>
                    new Event
                    {
                        VenueName = p.Name,
                        EventDate = p.EventDate,
                        AdminFee = p.PlayerAdminFee,
                        BuyinFee = p.PlayerBuyInFee,
                        ProFee = p.PlayerProFee,
                        DivisionName = p.DivisionName,
                        EventDescription = p.EventDescription,
                        GameID = p.GameID,
                        GameTypeName = p.evt_Game == null || p.evt_Game.evt_Discipline == null ? string.Empty : p.evt_Game.evt_Discipline.DisciplineName,
                        RegionName = p.RegionName,
                        StartTime = p.StartTime,
                        State = p.State
                    }).ToList();
                return result;
            }
        }
    }
}
