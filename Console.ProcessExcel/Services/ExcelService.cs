﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Excel;

namespace Console.ProcessExcel.Services
{
    public class ExcelService
    {
        private readonly string _filename;

        public ExcelService(string filename)
        {
            this._filename = filename;
        }

        public void WriteExcel<T>(List<T> source) where T : class
        {
            using (var workbook = new XLWorkbook(XLEventTracking.Disabled))
            {
                using (var writer = new CsvWriter(new ExcelSerializer(workbook)))
                {
                    writer.WriteRecords(source);
                }
                workbook.SaveAs(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),"Files",_filename));
            }
        }
    }
}
