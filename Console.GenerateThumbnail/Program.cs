﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var rootPath = @"C:\inetpub\wwwroot\FullHouseCMS_Poker\Resources\EventPhotos\";
            var files = Directory.GetFiles(rootPath, "*.*", SearchOption.AllDirectories)
                .Where(p =>
                {
                    var segments = p.Split(Path.DirectorySeparatorChar);
                    if (long.TryParse(segments[6], out var eventId) && eventId >= 486172)
                    {
                        var filename = Path.GetFileName(p);
                        if (string.IsNullOrEmpty(filename)) return false;
                        return !filename.EndsWith(".thumb") && !filename.StartsWith("Medium_") &&
                               !filename.StartsWith("Large_");
                    }
                    return false;

                }).ToList();
            Console.WriteLine($"Total files: {files.Count}");
            foreach (var file in files)
            {
                try
                {
                    Console.WriteLine($"{file} -- index: {files.IndexOf(file)} Out of {files.Count}");
                    ResizeImageForMediumAndLarge(Path.GetDirectoryName(file), Path.GetFileName(file));
                }
                catch (Exception e)
                {
                    continue;
                }
                
            }
            Console.WriteLine("Finish");
            Console.ReadLine();
        }        

        public static void ResizeImageForMediumAndLarge(string path, string originalFilename)
        {
            using (var image = Image.FromFile(Path.Combine(path, originalFilename)))
            {                
                foreach (var prop in image.PropertyItems)
                {
                    if (prop.Id == 0x0112)
                    {
                        int orientationValue = image.GetPropertyItem(prop.Id).Value[0];
                        RotateFlipType rotateFlipType = GetRotateFlipType(orientationValue);
                        image.RotateFlip(rotateFlipType);
                        break;
                    }
                }

                ResizeImage(image, path, originalFilename, 400, "Medium_");
                ResizeImage(image, path, originalFilename, 800, "Large_");
                image.Dispose();
            }            
        }

        public static void ResizeImage(Image image, string path, string filename, int width, string prefix)
        {
            var originalWidth = image.Width;
            var originalHeight = image.Height;
            // Figure out the ratio
            double ratio = (double)width / (double)originalWidth;
            // now we can get the new height and width
            int newHeight = Convert.ToInt32(originalHeight * ratio);
            System.Drawing.Image thumbnail =
                new Bitmap(width, newHeight); // changed parm names
            System.Drawing.Graphics graphic =
                System.Drawing.Graphics.FromImage(thumbnail);

            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;

            /* ------------------ new code --------------- */

            graphic.DrawImage(image, 0, 0, width, newHeight);

            /* ------------- end new code ---------------- */

            System.Drawing.Imaging.ImageCodecInfo[] info =
                ImageCodecInfo.GetImageEncoders();
            EncoderParameters encoderParameters;
            encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality,
                100L);
            thumbnail.Save(Path.Combine(path, prefix+filename), info[1],
                encoderParameters);
        }

        public static void ResizeImage(Image image, string originalFilename, int canvasWidth, int canvasHeight)
        {
            foreach (var prop in image.PropertyItems)
            {
                if (prop.Id == 0x0112)
                {
                    int orientationValue = image.GetPropertyItem(prop.Id).Value[0];
                    RotateFlipType rotateFlipType = GetRotateFlipType(orientationValue);
                    image.RotateFlip(rotateFlipType);
                    break;
                }
            }
            var originalWidth = image.Width;
            var originalHeight = image.Height;
            System.Drawing.Image thumbnail =
                new Bitmap(canvasWidth, canvasHeight); // changed parm names
            System.Drawing.Graphics graphic =
                         System.Drawing.Graphics.FromImage(thumbnail);

            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;

            /* ------------------ new code --------------- */

            // Figure out the ratio
            double ratioX = (double)canvasWidth / (double)originalWidth;
            double ratioY = (double)canvasHeight / (double)originalHeight;
            // use whichever multiplier is smaller
            double ratio = ratioX < ratioY ? ratioX : ratioY;

            // now we can get the new height and width
            int newHeight = Convert.ToInt32(originalHeight * ratio);
            int newWidth = Convert.ToInt32(originalWidth * ratio);

            // Now calculate the X,Y position of the upper-left corner 
            // (one of these will always be zero)
            int posX = Convert.ToInt32((canvasWidth - (originalWidth * ratio)) / 2);
            int posY = Convert.ToInt32((canvasHeight - (originalHeight * ratio)) / 2);

            graphic.Clear(Color.White); // white padding
            graphic.DrawImage(image, posX, posY, newWidth, newHeight);

            /* ------------- end new code ---------------- */

            System.Drawing.Imaging.ImageCodecInfo[] info =
                             ImageCodecInfo.GetImageEncoders();
            EncoderParameters encoderParameters;
            encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality,
                             100L);
            thumbnail.Save(originalFilename + ".thumb", info[1],
                             encoderParameters);
        }

        //=== determine image rotation
        public static RotateFlipType GetRotateFlipType(int rotateValue)
        {
            RotateFlipType flipType = RotateFlipType.RotateNoneFlipNone;

            switch (rotateValue)
            {
                case 1:
                    flipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    flipType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    flipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    flipType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    flipType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    flipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    flipType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    flipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    flipType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }

            return flipType;
        }
    }
}
